var path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'client', 'main.js'),
  output: {
    path: path.resolve(__dirname, 'client', 'build'),
    filename: 'build.js',
    publicPath: '/build/',
    chunkFilename: '[id].build.js?[chunkhash]'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    }, {
      test: /\.css$/,
      loader: 'style!css'
    }, {
      test: /\.less$/,
      loader: 'style!css!less'
    }, {
      test: /\.vue$/,
      loader: 'vue'
    }, {
      test: /\.(png|jpg|gif|svg|woff2?|eot|ttf)(\?.*)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        name: '[name].[ext]?[hash:7]'
      }
    }]
  },
  babel: {
    presets: ['es2015'],
    plugins: ['transform-runtime']
  },
  resolve: {
    extensions: ['', '.js', '.vue', '.less', '.css'],
    alias: {
      'views': path.resolve(__dirname, 'client', 'views'),
      'components': path.resolve(__dirname, 'client', 'components'),
      'styles': path.resolve(__dirname, 'client', 'styles')
    }
  }
}
