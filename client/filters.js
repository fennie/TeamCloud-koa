import moment from 'moment';

var filterPlugin = {};
filterPlugin.install = function(Vue, options) {
  Vue.filter('date', function(val) {
    return moment(val).format('YYYY-MM-DD HH:mm:ss');
  });
}

module.exports = filterPlugin;