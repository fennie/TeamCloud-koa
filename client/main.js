//require('normalize.css/normalize.css');
require('bootstrap-less/bootstrap/index');
require('font-awesome/css/font-awesome.css');
require('styles/basic');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import routerMap from './routers';
import filters from './filters';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(filters);

let router = new VueRouter({
  hashbang: true,
  history: false,
  saveScrollPosition: true,
  transitionOnLoad: true,
  linkActiveClass:'active'
});

routerMap(router);

router.beforeEach(transition => {
  if(transition.to.auth){
    if(localStorage.userId){
      transition.next();
    }else{
      transition.redirect('/login');
    }
  }else{
    transition.next();
  }
});

let app = Vue.extend({});

router.start(app,'#app');
