export default function(router) {
  router.map({
    '/login': {
      auth:'true',
      name: 'login',
      component: function(resolve) {require(['views/login'], resolve);},
    },
    '/':{
      name:'home',
      auth:'true',
      component: function(resolve) {require(['views/home'], resolve);},
      subRoutes:{
        '/personal': {
          name: 'personal',
          component: function(resolve) { require(['views/personal'], resolve); },
          subRoutes:{
            '/schedules':{
              name:'personal.schedules',
              component: function(resolve) { require(['views/personal_schedules'], resolve); }
            },
            // '/tasks':{
            //   name:'personal.tasks',
            //   component: function(resolve) { require(['views/personal_tasks'], resolve); }
            // }
          }
        },
        '/schedules': {
          name:'schedules',
          component: function(resolve) { require(['views/schedule'], resolve); }
        },
        '/projects':{
          name:'projects',
          component:function(resolve) {require(['views/projects'], resolve);},
        },
        '/teams':{
          component:function(resolve) {require(['views/blank'], resolve);},
          subRoutes:{
            '/':{
              name:'teams',
              component:function(resolve) {require(['views/teams'], resolve);},
            },
            '/add':{
              name:'teams.add',
              component:function(resolve){require(['views/teams_add'], resolve);}
            },
            '/:teamId':{
              name:'team',
              component:function(resolve){require(['views/team'], resolve);},
              subRoutes:{
                '/projects':{
                  component:function(resolve) {require(['views/blank'], resolve);},
                  subRoutes:{
                    '/':{
                      name:'team.projects',
                      component:function(resolve){require(['views/team_projects'], resolve);},
                    },
                    '/add':{
                      name:'team.projects.add',
                      component:function(resolve){require(['views/team_projects_add'], resolve);},
                    },
                    '/:projectId':{
                      name:'project',
                      component:function(resolve){require(['views/project'], resolve);},
                      subRoutes:{
                        '/tasks':{
                          name:'project.tasks',
                          component:function(resolve){require(['views/project_tasks'], resolve);},
                        },
                        '/members':{
                          name:'project.members',
                          component:function(resolve){require(['views/project_members'], resolve);},
                        },
                        '/documents':{
                          name:'project.documents',
                          component:function(resolve){require(['views/project_documents'], resolve);},
                        },
                      }
                    }
                  }
                },
                '/members':{
                  name:'team.members',
                  component:function(resolve){require(['views/team_members'], resolve);},
                }
              }
            }
          }
        }
      }
    }
  })
}
