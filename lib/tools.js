var crypto = require('crypto');

exports.apiError = function(msg) {
  return {
    status: false,
    msg: msg || '未知错误'
  };
}

exports.apiSuccess = function(data, msg) {
  return {
    status: true,
    msg: msg || '成功',
    data: data || null
  };
}

exports.md5 = function(text) {
  return crypto.createHash('md5').update(text).digest('hex');
}
