var Team = require('../models').Team;
var Project = require('../models').Project;
var User = require('../models').User;
var _ = require('lodash');

module.exports = function(router) {
  router.post('/project',function*() {
  	this.checkBody('teamId').notEmpty('请选择团队');
  	this.checkBody('name').notEmpty('项目名称不能为空');
  	if(this.errors){
  		this.body=this.apiError(this.errors);
  		return;
  	}

  	var teamId = this.request.body.teamId;
  	var name = this.request.body.name;
  	var admins = this.request.body.admins;
  	var members = this.request.body.members;
  	members=_.union(members,admins);
  	var team = yield Team.findById(teamId).exec();
  	if(!team){
  		this.body=this.apiError('该团队不存在')
  		return;
  	}
  	var project=new Project({
  		team:team._id,
  		name,
  		admins,
  		members
  	});
  	yield project.save();
  	this.body=this.apiSuccess({projectId:project._id});
  });

  router.get('/project/byTeamId',function*(){
  	var teamId = this.request.query.teamId;
  	if(!teamId){
  		this.body=this.apiError();
  		return;
  	}

  	var projects = yield Project.find({team:teamId}).lean().exec();
  	this.body=this.apiSuccess({projects});
  });

  router.get('/project/bySession',function*(){
    var userId = this.session.userId;
    if(!userId){
      this.body=this.apiError();
      return;
    }

    var teams = yield Team.find({members:userId}).exec();
    var projects = [];
    for(var i=0;i<teams.length;i++){
      var ps = yield Project.find({team:teams[i]._id}).populate('team').lean().exec();
      projects=projects.concat(ps);
    }
    
    this.body=this.apiSuccess({projects});
  });

  router.get('/project/:projectId',function*() {
  	var projectId=this.params.projectId;
  	if(!projectId){
  		this.body=this.apiError();
  		return;
  	}

  	var project=yield Project.findById(projectId).lean().exec();
  	if(!project){
  		this.body=this.apiError('该项目不存在');
  		return;
  	}
  	this.body=this.apiSuccess({project})
  });

  // 获取项目的成员
  router.get('/project/:projectId/members',function*(){
    var projectId = this.params.projectId;
    var project = yield Project.findById(projectId).populate('members').exec();
    if(!project){
      this.body = this.apiError('该项目不存在');
      return;
    }
    this.body = this.apiSuccess({project});
  })
  
  router.put('/project/member',function*(){
    var projectId=this.request.body.projectId;
    var userId=this.request.body.userId;
    var isAdmin=this.request.body.isAdmin;
    if(!projectId||!userId){
      this.body=this.apiError();
      return;
    }

    var project=yield Project.findById(projectId).exec();
    var user=yield User.findById(userId).exec();
    if(!project||!user){
      this.body=this.apiError('该项目或该用户不存在');
      return;
    }
    if(isAdmin==undefined){
      this.body=this.apiError();
      return;
    }

    if(isAdmin){
      project.admins.push(user._id);
      project.admins=_.uniq(project.admins);
    }
    else{
      for(var i=0;i<project.admins.length;i++){
        if(project.admins[i]==user._id+'')
          project.admins.splice(i,1);
      }
    }

    yield project.save();
    this.body=this.apiSuccess({isAdmin});
  });

};
