var Team = require('../models').Team;
var Project = require('../models').Project;
var User = require('../models').User;
var _ = require('lodash');

module.exports = function(router) {
  router.get('/team/getBySession',function*(){
    var userId = this.session.userId;
    var onlyAdmin = this.request.query.onlyAdmin; 
    if(!userId){
      this.body=this.apiError('请登录');
      return;
    }

    var query = {}
    if(onlyAdmin){
      query['$or']=[
        {superAdmin:userId},
        {admins:userId}
      ]
    }else{
      query['members']=userId;
    }
    var teams = yield Team.find(query).exec();

    this.body=this.apiSuccess({teams});
  });

  router.get('/team/project',function*(){
    var userId = this.session.userId;
    var onlyAdmin = this.request.query.onlyAdmin; 
    if(!userId){
      this.body=this.apiError('请登录');
      return;
    }

    var query = {}
    if(onlyAdmin){
      query['$or']=[
        {superAdmin:userId},
        {admins:userId}
      ]
    }else{
      query['members']=userId;
    }
    var projects = yield Project.find(query).populate('team').exec();
    
    this.body=this.apiSuccess({projects});
  });

  router.get('/team/:teamId',function*(){
    var teamId=this.params.teamId;
    var userId=this.session.userId;
    if(!teamId){
      this.body = this.apiError();
      return;
    }

    var team = yield Team.findById(teamId).populate('members').lean().exec();
    if(!team){
      this.body = this.apiError('找不到该团队');
      return;
    }
    if(team.superAdmin==userId){
      team.isSuperAdmin=true;
    }
    if(_.findIndex(team.admins,function(au){return au==userId})>-1){
      team.isAdmin=true;
    }
    if(_.findIndex(team.members,function(mu){return mu._id==userId})>-1){
      team.isMember=true;
    }

    this.body=this.apiSuccess({team});
  });

  /*创建团队*/
  router.post('/team', function*() {
  	var name = this.request.body.name;
    var members = this.request.body.members;
    var userId = this.session.userId;
  	if(!name||!userId){
  		this.body = this.apiError();
  		return;
  	}

  	var teamExist = yield Team.findOne({name}).exec();
  	if(teamExist){
  		this.body = this.apiError('团队已经存在，请修改');
  		return;
  	}

    members = members.map(m=>{ return m._id; });
    members = _.union(members,[userId]);

    var team = new Team({
      name,
      creator:userId,
      superAdmin:userId,
      members
    });

    yield team.save();

  	this.body = this.apiSuccess({teamId:team._id});
  });

  router.put('/team/member',function*(){
    var teamId=this.request.body.teamId;
    var userId=this.request.body.userId;
    var isAdmin=this.request.body.isAdmin;
    if(!teamId||!userId){
      this.body=this.apiError();
      return;
    }

    var team=yield Team.findById(teamId).exec();
    var user=yield User.findById(userId).exec();
    if(!team||!user){
      this.body=this.apiError('该团队或该用户不存在');
      return;
    }
    if(isAdmin==undefined){
      this.body=this.apiError();
      return;
    }

    if(isAdmin){
      team.admins.push(user._id);
      team.admins=_.uniq(team.admins);
    }
    else{
      for(var i=0;i<team.admins.length;i++){
        if(team.admins[i]==user._id+'')
          team.admins.splice(i,1);
      }
    }

    yield team.save();
    this.body=this.apiSuccess({isAdmin});
  });

};
