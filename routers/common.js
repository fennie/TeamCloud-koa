var User = require('../models').User;
var tools = require('../lib/tools');

module.exports = function(router) {
  //登录
  router.post('/login', function*() {
    this.checkBody('username').notEmpty('用户名不能为空');
    this.checkBody('password').notEmpty('密码不能为空');
    if (this.errors) {
      this.body = this.apiError(this.errors);
      return;
    }

    var username = this.request.body.username;
    var password = tools.md5(this.request.body.password);
    var user = yield User.findOne({ username }).exec();
    if (!user) {
      this.body = this.apiError('用户名不存在');
      return;
    }
    if (password !== user.password) {
      this.body = this.apiError('密码不正确');
      return;
    }
    this.session.username = user.username;
    this.session.userId = user._id;

    this.body = this.apiSuccess({
      username: user.username,
      userId: user._id
    });
  });

  //注册
  router.post('/signIn', function*() {
    this.checkBody('username').notEmpty('用户名不能为空');
    this.checkBody('password').notEmpty('密码不能为空');
    if (this.errors) {
      this.body = this.apiError(this.errors);
      return;
    }

    var username = this.request.body.username;
    var password = tools.md5(this.request.body.password);
    var userExist = yield User.findOne({
      username: username
    }).exec();
    if (userExist) {
      this.body = this.apiError('用户名已存在');
      return;
    }

    var user = new User({
      username: username,
      password: password
    });
    yield user.save();

    this.body = this.apiSuccess();
  });
};
