var parse = require('co-busboy');
var fs = require('fs');
var os = require('os');
var path = require('path');

var Project = require('../models').Project;
var Document = require('../models').Document;
var User = require('../models').User;

module.exports = function(router) {
	router.get('/project/:projectId/documents',function*(){
	    var projectId=this.request.query.projectId;
	    if(!projectId){
	      this.body=this.apiError();
	      return;
	    }

	    var documents = yield Document.find({project: projectId}).exec();
	    this.body=this.apiSuccess({documents});
	});

	router.delete('/document/:URL/:id',function*(){
		console.log(this)
		console.log(this.request.body);
		console.log(this.request.query);
		var document = yield Document.find({_id:this.request.params.id}).exec();
		if(!document){
			this.body = this.apiError({});
		}
		document.remove();
		this.body = this.apiSuccess({});
	});

	router.post('/teams/:teamId/projects/:projectId/documents',function*(){
		if (!this.request.is('multipart/*')){
			return yield next;
		}
		var str = this.request.url;
		var teamId = str.substring(str.indexOf("/teams/")+7,str.indexOf("/projects/"));
		var projectId = str.substring(str.indexOf("/projects/")+10,str.indexOf("/documents"));

	    if(!projectId){
	      this.body=this.apiError();
	      return;
	    }

	    var project = yield Project.findById(projectId).exec();
	    if(!project){
	      this.body=this.apiError('项目不存在');
	      return;
	    }

	    var parts = parse(this);
	    var part;
	    var document;

	    while (part = yield parts) {
    		var stream = fs.createWriteStream(path.join(__dirname,'../client/upload/'+part.filename));
		    part.pipe(stream);
		   // console.log('uploading %s -> %s', part.filename, stream.path);
		    document=new Document({
		    	project: projectId,
		    	name:part.filename,
		    	author:this.session.userId,
		    	url:'/upload/'+part.filename,
		    	URL:path.join(__dirname,'../client/upload/'+part.filename),
		    })
	    	yield document.save();
		}

	  // this.body=this.apiSuccess({});
	  var url = "#!/teams/"+teamId+"/projects/"+projectId+"/documents";
	  this.redirect(url);
	});
}