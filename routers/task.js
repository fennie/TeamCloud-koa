var Project = require('../models').Project;
var Task = require('../models').Task;
var User = require('../models').User;

module.exports = function(router) {
  //获取项目的任务
  router.get('/task/getByProjectId',function*(){
    var projectId=this.request.query.projectId;
    if(!projectId){
      this.body=this.apiError();
      return;
    }

    var tasks = yield Task.find({project: projectId}).populate('receiver').exec();
    this.body=this.apiSuccess({tasks});
  });

  //获取任务的时间
  router.get('/task/getByDate',function*(){
    var tasks = yield Task.find({isCompleted:false,receiver:this.session.userId}).exec();
    for(var i=0;i<tasks.length;i++){
      if(!tasks[i].endDate){
        tasks.splice(i);
      }
    }
    this.body=this.apiSuccess({tasks});
  });

  //创建任务
  router.post('/task', function*() {
    var content=this.request.body.content;
    var receiver=this.request.body.receiver;
    var projectId=this.request.body.projectId;
    var userId=this.session.userId;
    if(!content||!projectId||!userId){
      this.body=this.apiError();
      return;
    }

    var project = yield Project.findById(projectId).exec();
    if(!project){
      this.body=this.apiError('项目不存在');
      return;
    }

    var task = new Task({
      project:projectId,
      creator:userId,
      content,
      receiver,
    });

    yield task.save();

    this.body=this.apiSuccess({task});
  });
  //修改任务
  router.put('/task/:taskId',function*(){
    var taskId = this.params.taskId;
    var content = this.request.body.content;
    var receiverId = this.request.body.receiverId;
    var isCompleted = this.request.body.isCompleted;
    var endDate = this.request.body.endDate;

    if(!taskId){
      this.body=this.apiError();
      return;
    }

    var task = yield Task.findById(taskId).exec();
    if(!task){
      this.body=this.apiError('该任务不存在');
      return;
    }

    if(content!=undefined)
      task.content=content;
    if(receiverId!=undefined){
      var receiver=yield User.findById(receiverId).exec();
      if(!receiver){
        this.body=this.apiError('该用户不存在');
        return;
      }
      task.receiver=receiver._id;
    }
    if(isCompleted!=undefined){
      task.isCompleted=isCompleted;
    }
    if(endDate!=undefined){
      task.endDate = endDate;
    }

    yield task.save();
    this.body=this.apiSuccess({task});
  });
  //删除任务
  router.delete('/task/:taskId',function*(){
    var taskId = this.params.taskId;

    if (!taskId) {
      this.body = this.apiError();
      return;
    }
    yield Task.findOneAndRemove({_id:taskId}).exec();
    this.body = this.apiSuccess();
  });
};
