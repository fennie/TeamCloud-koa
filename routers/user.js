var User = require('../models').User;
var Team = require('../models').Team;
var Project = require('../models').Project;
var tools = require('../lib/tools');
var _ = require('lodash');

module.exports = function(router) {
  router.get('/user/getByUsername', function*() {
  	var username = this.request.query.username;
  	if(!username){
  		this.body = this.apiError('用户名不能为空');
  		return;
  	}

  	var user = yield User.findOne({username:username}).exec();
  	if(!user){
  		this.body = this.apiError('用户名不存在');
  		return;
  	}
  	this.body = this.apiSuccess({
  		_id:user._id,
  		username:user.username
  	})
  });

  router.get('/user/getByTeamId',function*() {
  	var teamId = this.request.query.teamId;
  	if(!teamId){
  		this.body = this.apiError();
  		return;
  	}

  	var team = yield Team.findById(teamId).populate('members').lean().exec();
  	if(!team){
  		this.body = this.apiError('找不到该团队');
  		return;
  	}
    var users=team.members;
    var superAdmin=team.superAdmin;
    var admins=team.admins;
    users.forEach(u=>{
      if(u._id==superAdmin+'')
        u.isSuperAdmin=true;
      else
        u.isSuperAdmin=false;
      if(_.findIndex(admins,au=>{return u._id==au+''})>-1)
        u.isAdmin=true;
      else
        u.isAdmin=false;
    });

  	this.body = this.apiSuccess({users});
  });

  router.get('/user/getByProjectId',function*() {
    var projectId = this.request.query.projectId;
    if(!projectId){
      this.body = this.apiError();
      return;
    }

    var project = yield Project.findById(projectId).populate('members').lean().exec();
    if(!project){
      this.body = this.apiError('找不到该项目');
      return;
    }
    var users = project.members;
    var admins = project.admins;
    users.forEach(u=>{
      if(_.findIndex(admins,au=>{return u._id==au+''})>-1)
        u.isAdmin=true;
      else
        u.isAdmin=false;
    });

    this.body = this.apiSuccess({users});
  });

  router.get('/personal/tasks',function*(){
    
  });
};
