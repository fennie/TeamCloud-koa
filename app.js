var app = require('koa')();
var logger = require('koa-logger');
var views = require('koa-views');
var serve = require('koa-static');
var session = require('koa-session');
var router = require('koa-router')();
var mongoose = require('mongoose');
var parse = require('co-busboy');
var os = require('os');
var path = require('path');
var config = require('./config');
var tools = require('./lib/tools');

// log requests
//app.use(logger());

//mongodb
mongoose.connect(config.mongodb);
mongoose.connection.on('error',function(err){
  console.log();
  console.error(err);
  process.exit(1);
});

//app
app.on('error',function(err){
  console.log(err);
});

app.config = config;
app.keys=[config.secret];
app.use(session(app));
app.use(views(path.join(__dirname,'client'),{map:{html:'ejs'}}));
app.use(serve(path.join(__dirname,'client'))); //static

app.use(function*(next){
  this.apiError=tools.apiError;
  this.apiSuccess=tools.apiSuccess;
  yield next;
});

app.use(require('koa-body')());
app.use(require('koa-validate')());

//加载路由
var fs = require('fs');
var files = fs.readdirSync(path.join(__dirname,'routers'));
files.forEach(function(filename){
  require('./routers/'+filename)(router);
});

router.get('/',function*(next) {
  yield this.render('index');
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(8080,function(){
  console.log('server started on port '+ 8080);
});
