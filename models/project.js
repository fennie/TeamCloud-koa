var mongoose = require('mongoose');
	Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var ProjectSchema = new Schema({
	/*团队*/
	team: {type: ObjectId, ref: 'Team'},
	/*项目名*/
	name: { type: String },
	/*创建时间*/
	createAt: { type: Date, default:Date.now},
	/*管理员*/
	admins: [{ type: ObjectId, ref: 'User'}],
	/*成员*/
	members: [{ type: ObjectId, ref: 'User'}] ,
});

module.exports = mongoose.model('Project',ProjectSchema);