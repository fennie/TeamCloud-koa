var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var TaskSchema = new Schema({
	project:{type:ObjectId,ref:'Project'},
	creator:{type:ObjectId,ref:'User'},
	receiver:{type:ObjectId,ref:'User'},
	content:{type:String},
	isCompleted:{type:Boolean,default:false},
	createAt:{type:Date,default:Date.now},//创建任务的时间
	endDate:{type:String},//任务截止时间
});

module.exports = mongoose.model('Task',TaskSchema);