var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  username:{type:String},
  password:{type:String},
  createAt:{type:Date,defaults:Date.now},
});

module.exports = mongoose.model('User',UserSchema);
