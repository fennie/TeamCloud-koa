var mongodb = require('../config/mongodb'),
	mongoose = mongodb.mongoose,
	Schema = mongoose.Schema;

var PictureSchema = new Schema({
	/*图片名*/
	fileName: {
		type: String
	},
	/*上传时间*/
	creatAt: {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('Picture', PictureSchema);