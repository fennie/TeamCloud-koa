var mongodb = require('../config/mongodb'),
	mongoose = mongodb.mongoose,
	Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var ShareSchema = new Schema({
	/*图片名*/
	fileName: { type: String },
	/*上传者*/
	creator: { type: ObjectId, ref: 'User' },
	/*上传时间*/
	creatAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Share', ShareSchema);