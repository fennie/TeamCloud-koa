var mongoose = require('mongoose');
	Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var TeamSchema = new Schema({
	/*团队名*/
	name: { type: String },
	/*创建时间*/
	createAt: { type: Date, default: Date.now },
	/*创始人*/
	creator: { type: ObjectId, ref: 'User' },
	/*超级管理员*/
	superAdmin: { type: ObjectId, ref: 'User' },
	/*管理员*/
	admins: [{ type: ObjectId, ref: 'User' }],
	/*成员*/
	members: [{ type: ObjectId, ref: 'User' }],
});

module.exports = mongoose.model('Team',TeamSchema);