var mongoose = require('mongoose');
	Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var DocumentSchema = new Schema({
	/*团队*/
	team: {type: ObjectId, ref: 'Team'},
	/*项目*/
	project: {type: ObjectId, ref: 'Project'},
	/*文件名*/
	name: { type: String },
	/*相对文件路径*/
	url: { type: String },
	/*绝对文件路径*/
	URL: { type: String },
	/*上传时间*/
	uploadDate: { type: Date, default: Date.now},
	/*上传人员*/
	author: { type: ObjectId, ref: 'User'},
});

module.exports = mongoose.model('Document',DocumentSchema);