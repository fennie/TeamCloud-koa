var mongodb = require('../config/mongodb'),
	mongoose = mongodb.mongoose,
	Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

var ScheduleSchema = new Schema({
	_id: { type: ObjectId, ref: 'User'},
	/*日程题目*/
	title: { type: String },
	/*日程内容*/
	content: { type: String }
});

module.exports = mongoose.model('Schedule',ScheduleSchema);